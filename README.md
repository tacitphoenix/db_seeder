# Database Seeder

Have you ever created a new database and needed to seed it with information? Look no further
*Database Seeder* is here to help you quickly import data in CSV and JSON format into your
database. You can also simulate and API by reading the files into memory and interacting with
them.

You have a new database now you have the **data**!
